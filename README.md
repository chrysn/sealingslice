Sealing Slice
=============

This crate impements a slice that can seal its initial part off from mutability, and hand out
the sealed parts as immutable references.

Crate dimensions
----------------

This is a very small crate, with only about a dozen lines of effective code.

It is still provided in a dedicated crate to clearly encapsulate the guarantees
of this crate from whatever uses it, and to provide some testing and stability,
as opposed to ad-hoc implementations of the same concept.

All unsafety is encapsulated in the single function `coalesce`, which asserts
that its argument slices form a contiguous area in memory and can therefore be
viewed as a single slice. The SealingSlice construction around it ensures that
this assertion is not violated.

When (not) to use
-----------------

This crate should only be used if the standard library does not provide a safe
way to the desired splitting.

An indicator that you'd actually need to use this crate is that you need to
have long-lived immutable slices of your data, and later need a slice that
encompasses both that earlier data and data that was still being mutated when
the first one was requested.

If you merely need to split off parts of your data after you're done mutating
them, and don't need to access earlier or overlapping regions thereof, [slice::split\_at\_mut](https://doc.rust-lang.org/std/primitive.slice.html#method.split_at_mut)
is your friend. If your splitting-off function is a method of a struct that
keeps a reference to your slice, you might need to temporarily move your
mutable slice out of it (and formally leave a `&[]` in its place) to keep the
full life-times. See [issue 1](https://gitlab.com/chrysn/sealingslice/issues/1)
for an example. In those cases, you *do not need to use* this crate.

Roadmap
-------

This crate could be versioned as 1.0 soon, given there's not much interface
that can change.

Variations that were considered on the API:

* Implement the Index and IndexMut operation on the SealingSlice. Access via an
  immutable indexing would panic out-of-bounds when access after the seal is
  attempted (especially via the end-open interval for a not-completely-sealed
  slice), and mutable indexing would similarly err out if sealed areas are
  accessed.

  For users that access mutable areas at fixed offsets, this would mean less
  calculating their position in the current mutable view.

  Implementation would, line-of-code-wise, exceed the current code, as all
  variations of Range, RangeFrom, RangeToInclusive etc. would need to be
  covered.

* If need for slices that seal a different part than their first bytes (eg.
  their tail, a single contiguous region or even an arbitrary number of areas)
  arises, those would make sense to be added to this crate (as the evaluation
  of their safety properties could be shared), but that need has not come up
  yet.

The former can still be implemented with this crate stabilizing; the latter is
probably too different and could need to be implemented separately.

Crate status
------------

The author's main use case for having this crate ([enabling no\_std use of serde-cbor](https://github.com/pyfisch/cbor/pull/81)
has gone away since a reviewer pointed out an alternative way of implementation in [issue #1](https://gitlab.com/chrysn/sealingslice/issues/1).
Having SealingSlice around can still be valuable, so this crate is kept alive,
but -- depending on how other use cases develop -- the crate is declared as
"passively maintained" for now.
